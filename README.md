Bonobo (Symfony example)
========================

Features :
   - Friend list
   - Profile
   - Food preferences
   - Race and Family of the monkeys

Getting started
--------------
* install with composer `composer install`
* install the application (app/config/parameters)
* generate the database `bin/console doctrine:database:create`
* load fixtures `bin/console doctrine:fixtures:load`


[1]:  https://symfony.com/doc/3.2/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.2/doctrine.html
[8]:  https://symfony.com/doc/3.2/templating.html
[9]:  https://symfony.com/doc/3.2/security.html
[10]: https://symfony.com/doc/3.2/email.html
[11]: https://symfony.com/doc/3.2/logging.html
[12]: https://symfony.com/doc/3.2/assetic/asset_management.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
