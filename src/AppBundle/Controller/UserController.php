<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Food;
use AppBundle\Entity\User;
use AppBundle\Form\FoodType;
use AppBundle\Form\UserType;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     * Not the connected user
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')
            ->findAllWithAssociationsAndNot($this->getUser()->getId());

        return $this->render('user/index.html.twig', array(
            'users' => $users,
            'my_friends' => $this->getUser()->getMyFriends(),
        ));
    }

    /**
     * Add an User to the FriendsList
     *
     * @Route("/add-friend/{user_id}", name="user_add_friend", requirements={"user_id": "\d+"})
     * @Method({"GET"})
     * @param $user_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addFriendAction($user_id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $friend = $em->getRepository('AppBundle:User')->find($user_id);

        if (!($friend instanceof User)) {
            throw new NotFoundHttpException("Not found User.");
        }

        if (!$this->getUser()->getMyFriends()->contains($friend)) {

            $this->getUser()->addMyFriend($friend);
            $em->flush();

            $this->addFlash('success', 'Ajouté à la liste de vos amis.');
        } else {
            $this->addFlash('warning', 'Déjà dans votre liste d\'amis.');
        }


        return $this->redirectToRoute('user_index');
    }

    /**
     * Remove an User from the FriendsList
     *
     * @Route("/remove-friend/{user_id}", name="user_remove_friend", requirements={"user_id": "\d+"})
     * @Method({"GET"})
     * @param $user_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeFriendAction($user_id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $friend = $em->getRepository('AppBundle:User')->find($user_id);

        if (!($friend instanceof User)) {
            throw new NotFoundHttpException("Not found User.");
        }

        if ($this->getUser()->getMyFriends()->contains($friend)) {

            $this->getUser()->removeMyFriend($friend);
            $em->flush();

            $this->addFlash('success', 'Supprimé de la liste de vos amis.');
        } else {
            $this->addFlash('warning', 'Absent de votre liste d\'amis.');
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Lists all user entities that are in the FriendList.
     */
    public function indexFriendsAction()
    {
        return $this->render('partials/friends.html.twig', [
            'my_friends' => $this->getUser()->getMyFriends(),
        ]);
    }

    /**
     * Add a food to the user's food.
     *
     * @Route("/add-food", name="user_add_food")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addFoodAction(Request $request)
    {
        $form = $this->createForm(FoodType::class, new Food(), [
            'action' => $this->generateUrl('user_add_food'),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();

            $name = $form->getData()->getName();
            $food = $em->getRepository('AppBundle:Food')->findOneByName($name);

            if (!($food instanceof Food)) {
                $food = new Food();
                $food->setName($name);
                $em->persist($food);
            }

            if (!$food->getUser()->contains($food)) {

                $food->addUser($this->getUser());
                $em->flush();

                $this->addFlash('success', 'Plat ajouté à vos plats favoris.');
            } else {
                $this->addFlash('warning', 'Le plat est déjà dans vos plats favoris.');
            }
        }

        return $this->redirectToRoute('user_edit_profile');
    }

    /**
     * Remove a food from the user's food.
     *
     * @Route("/remove-food/{food_id}", name="user_remove_food", requirements={"food_id": "\d+"})
     * @Method({"GET"})
     * @param $food_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeFoodAction($food_id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $food = $em->getRepository('AppBundle:Food')->find($food_id);

        if (!($food instanceof Food)) {
            throw new NotFoundHttpException("Not found Food.");
        }

        if ($this->getUser()->getFood()->contains($food)) {

            $food->removeUser($this->getUser());

            $em->flush();

            $this->addFlash('success', 'Plat supprimé de vos plats favoris.');
        } else {
            $this->addFlash('warning', 'Le plat n\'est pas dans vos plats favoris.');
        }

        return $this->redirectToRoute('user_edit_profile');
    }

    /**
     * User's profile edition.
     *
     * @Route("/edit-profile", name="user_edit_profile")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editProfileAction(Request $request)
    {
        $editForm = $this->createForm(UserType::class, $this->getUser(), [
            'action' => $this->generateUrl('user_edit_profile')
        ]);

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Profil édité.');
            return $this->redirectToRoute('user_edit_profile');
        }

        return $this->render('user/edit_profile.html.twig', array(
            'user' => $this->getUser(),
            'edit_form' => $editForm->createView(),
            'form_food' => $this->createForm(FoodType::class, new Food(), [
                'action' => $this->generateUrl('user_add_food'),
            ])->createView(),
        ));
    }
}
