<?php
// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Family;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Race;

class LoadRacesData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $races = [
            "allocèbe", "alouate", "angwantibo", "apelle", "atèle", "avahi", "aye-aye", "babouin", "bandar", "bonobo", "callicèbe", "callitriche", "singe capucin", "cercocèbe", "cercopithèque", "chimpanzé", "chirogale", "colobe", "cynocéphale", "douc", "douroucouli", "drill", "entelle", "galago", "gélada", "gibbon", "gorille", "grivet", "guenon", "guéréza", "hocheur", "houlock", "singe hurleur"
        ];

        foreach ($races as $key => $race) {
            $raceD = new Race();
            $raceD->setName($race);

            $manager->persist($raceD);
            for ($i = 0; $i < 10; $i++) {
                $family = new Family();
                $family->setRace($raceD);
                $family->setName("famille-" . $i . '-' . $raceD->getName());
                $manager->persist($family);
            }

        }
        $manager->flush();
    }
}