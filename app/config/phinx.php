<?php
$parameters = \Symfony\Component\Yaml\Yaml::parse(file_get_contents('parameters.yml'));

return array(
    'paths' => array(
        'migrations' => __DIR__ . '/db/migrations'
    ),
    'environments' => array(
        'default_migration_table' => 'migration',
        'default_database' => 'prod',
        'prod' => array(
            'adapter' => 'mysql', // Could also be "mysql", "sqlite" or "sqlsrv"
            'host' => $parameters['parameters']['database_host'],
            'name' => $parameters['parameters']['database_name'],
            'user' => $parameters['parameters']['database_user'],
            'pass' => $parameters['parameters']['database_password'],
            'port' => $parameters['parameters']['database_port'],
            'charset' => 'utf8',
        ),
        'test' => array(
            'adapter' => 'mysql',
            'host' => $parameters['parameters']['database_host'],
            'name' => $parameters['parameters']['database_name'] . '_test',
            'user' => $parameters['parameters']['database_user'],
            'pass' => $parameters['parameters']['database_password'],
            'port' => $parameters['parameters']['database_port'],
            'charset' => 'utf8',
        ),
    ),
);